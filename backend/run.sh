#!/bin/sh
. venv/bin/activate
python /app/manage.py makemigrations
python /app/manage.py migrate
uwsgi --socket /var/run/nginx.sock --module backend.wsgi --chmod-socket=666
