from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
import json

# Create your views here.
def login_view(request):
    """View that authenticates user"""
    if request.method == "POST":
        data = json.loads(request.body.decode("utf-8"))
        try:
            username = data["username"]
            password = data["password"]
        except KeyError:
            return JsonResponse(
                {"success": False, "message": "Brak odpowiednich danych w zapytaniu."}
            )
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirect to a success page.
            return JsonResponse({"success": True})
        return JsonResponse({"success": False, "message": "Złe hasło lub login."})
    return JsonResponse(
        {"success": False, "message": "Metoda get nie jest wspierana dla /login."}
    )

def logout_view(request):
    logout(request)
    return JsonResponse({"success": True})
