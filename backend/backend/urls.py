"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .settings import URL_PREFIX
from .views import request_csrf_cookie_view

urlpatterns = [
    path(f"{URL_PREFIX}admin/", admin.site.urls),
    path(f"{URL_PREFIX}authentication/", include("authentication.urls")),
    path(f"{URL_PREFIX}csrf_cookie/", request_csrf_cookie_view),
    path(f"{URL_PREFIX}restapi/", include("restapi.urls")),
]
