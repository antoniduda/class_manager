from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import JsonResponse
import json

# Create your views here.
def csrf_error_view(request, reason):
    """View for when users csrf token fails"""
    return JsonResponse({"success": False, "error_code": 0}, status=403)

@ensure_csrf_cookie
def request_csrf_cookie_view(request):
    """Gives user new csrf cookie"""
    return JsonResponse({})
