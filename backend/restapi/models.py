from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Meeting(models.Model):
    """
    Model for meetings
    Includes: id, user as foreign key, date, title - varchar of length 250, present.
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField()
    title = models.CharField(max_length=250)
    present = models.IntegerField()

    class Meta:
        ordering = ["-date"]
