from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from restapi import views

urlpatterns = [
    path('meetings/', views.MeetingViewSet.as_view()),
    path('meetings/<pk>', views.MeetingEditSet.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
