from rest_framework import serializers
from restapi.models import Meeting
from django.core.exceptions import PermissionDenied
import logging
import datetime

logger = logging.getLogger(__name__)

class MeetingSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(read_only=True, slug_field="username")
    class Meta:
        model = Meeting
        fields = ['id', 'user', 'date', 'title', 'present']
        read_only_fields = ['id']

    def create(self, data):
        request = self.context.get("request")
        meeting = Meeting(**data)
        if request and hasattr(request, "user"):
            meeting.user = request.user
            meeting.save()
            return meeting
        logger.error("You forgot to pass request in context")
        raise PermissionDenied

    def update(self, obj, data):
        try:
            obj.title = data['title']
        except KeyError:
            pass
        try:
            obj.date = data['date']
        except KeyError:
            pass
        try:
            obj.present = data['present']
        except KeyError:
            pass
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            obj.user = request.user
            obj.save()
            return obj
        logger.error("You forgot to pass request in context")
        raise PermissionDenied
