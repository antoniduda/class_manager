from django.shortcuts import render
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework import permissions
from .serializers import MeetingSerializer
from .models import Meeting
from django.db.models import Q
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.http import JsonResponse


def formatted_response(data, success, view=None):
    if view:
        return view.get_paginated_response(data)
    ret_data = {}
    ret_data["success"] = success
    ret_data["results"] = data
    return Response(ret_data)


# Create your views here.
class MeetingViewSet(APIView, PageNumberPagination):
    authentication_classes = [SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    page_size = 10

    def get(self, request, format=None):
        if request.user.is_staff:
            meetings = Meeting.objects.all()
        else:
            meetings = Meeting.objects.filter(user=request.user)
        paginated = self.paginate_queryset(meetings, request, view=self)
        serializer = MeetingSerializer(paginated, many=True)
        return formatted_response(serializer.data, True, self)

    def post(self, request, format=None):
        meeting = MeetingSerializer(
            data=request.data, context={"request": request}
        )
        if meeting.is_valid():
            meeting.save()
            return JsonResponse(
                {
                    "success": True,
                    "id": meeting.data["id"],
                    "user": meeting.data["user"],
                }
            )
        return JsonResponse(
            {"success": False, "message": "Nieprawidłowe dane"}
        )


class MeetingEditSet(APIView, PageNumberPagination):
    authentication_classes = [SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def put(self, request, pk, format=None):
        try:
            meeting = Meeting.objects.get(pk=pk)
            if meeting.user == request.user or request.user.is_staff:
                serializer = MeetingSerializer(
                    meeting,
                    data=request.data,
                    partial=True,
                    context={"request": request},
                )
                if serializer.is_valid():
                    serializer.save()
                    return formatted_response(serializer.data, True)
            raise PermissionDenied
        except Meeting.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        try:
            meeting = Meeting.objects.get(pk=pk)
            if meeting.user == request.user or request.user.is_staff:
                meeting.delete()
                return JsonResponse({"success": True})
            raise PermissionDenied
        except Meeting.DoesNotExist:
            raise Http404
