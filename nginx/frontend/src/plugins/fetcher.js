export default {
  install: (Vue, options) => {
    var getCSRFToken = () => {
      var name = "csrftoken"
      let cookieValue = null;
      if (document.cookie && document.cookie !== '') {
          const cookies = document.cookie.split(';');
          for (let i = 0; i < cookies.length; i++) {
              const cookie = cookies[i].trim();
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) === (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
    }
    var errorStatusHandler = function(response) {
      if(response.status==403)
        alert("Brak uprawnień");
    }
    var errors = Array();
    errors.push(async (url, params) => {
      if(!params.headers)
        params.headers = {}
      await fetch(options + "csrf_cookie/", {
        method: "GET",
      },
      ).catch(err => console.log(err));
      var csrf = getCSRFToken();
      params.headers["X-CSRFToken"] = csrf;
      params.headers["Content-Type"] = "application/json";
      var data = await fetch(url, params)
      errorStatusHandler(data);
      var resolved = await data.json()
      return resolved;
    })

    var handleError = (url, params, data) => {
      return errors[data.error_code](url, params);
    }

    Vue.config.globalProperties.$apiUrl = options
    Vue.config.globalProperties.$fetch = async (url, params) => {
      if(!params.headers)
        params.headers = {}
      if(params.method !="GET") {
        var csrf = getCSRFToken()
        if(csrf == null) {
          await fetch(options + "csrf_cookie/", {
            method: "GET",
          },
          ).catch(err => console.log(err));
          if(csrf = getCSRFToken(), csrf == null) {
            console.log("Something went terribly wrong")
          }
        }
        params.headers["X-CSRFToken"] = csrf;
      }
      params.headers["Content-Type"] = "application/json";
      var data = await fetch(url, params)
      errorStatusHandler(data);
      var resolved = await data.json()
      if(resolved.success != undefined && resolved.error_code != undefined)
        return handleError(url, params, resolved);
      return resolved;
  }
  },
}
