import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import fetcher from './plugins/fetcher'
import store from './store/index.js'
import './index.css'


const apiUrl = "http://127.0.0.1/api/"

createApp(App).use(router).use(store).use(fetcher, apiUrl).mount('#app')
