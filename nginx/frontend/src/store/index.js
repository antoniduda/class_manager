import {createStore} from 'vuex'
import VuexPersistence from 'vuex-persist'


export default createStore({
  state: {
    loggedIn: false
  },
  mutations: {
    LOGIN_TOGGLE (state) {
      state.loggedIn = !state.loggedIn;
    }
  },
  plugins: [new VuexPersistence().plugin]
})
