# configuration of the server
server {
    # the port your site will be served on
    listen      8000;
    # the domain name it will serve for
    server_name example.com; # substitute your machine's IP address or FQDN
    charset     utf-8;

    # max upload size
    client_max_body_size 75M;   # adjust to taste

    # Django media
    location /  {
        alias /serve/;  # your Django project's media files - amend as required
    }
    location /api {
        include     ./uwsgi_params; # the uwsgi_params file you installed
        uwsgi_pass  unix:/var/run/nginx.sock;
    }
}
